# Vortrag "Digitale Selbstverteidigung bei der KJRS (Kinder- und Jugendring Sachsen)
Vorbereitungstreffen: 05.12.2019  
Talk: 11.12.2019  

## Fragen
- Wie weit haben Apps, die man nutzt, Zugriff darauf, was ich in anderen Apps tue oder speichere?  
- Wenn ich Bilder für Apps wie WhatsApp freigebe, kann die App dann immer darauf zugreifen und evtl.   sogar an den Anbieter hochladen oder ist der Zugriff nur lokal, als die Erlaubnis einzelne ausgewählte Bilder hochzuladen?  
- Wie funktioniert das mit den Cookies, ist personalisierte Werbung schlimm und was wissen Google, Amazon und Co. über uns? Data-Mining...  
- Wie mache ich ein Backup der Daten vom Smartphone, wie fange ich an, was kann in den Alltag übergehen?  
- Never change a running system vs. Updates für Betriebssysteme auf Smartphones / Notebooks. Verständnis dafür, dass es nicht nur um Funktionen, sondern auch Sicherheitslücken geht und Updates Sinn machen  

sicher Surfen
Datensicherheit
Datenschutz Mobile - Apps
Updates

Jahresausklang für die KJRS

## Ablauf
- Zeit? 1h von 17 
- Wlan ssid sniffer
- ich ab 16:45


# Klientel
- MitarbeiterInnen des KJRS 
- ca. 15 Menschen

# Ort
Gaststätte in Radebeul
Hauptstr. 19 
Gräfes Wein & feines


## Inhalte
- Datenschutz Allg. / gesetzliche Regelungen
- Alternativen

# Mitbringen
- Stormverteiler 3er
- Notebook
- foo

