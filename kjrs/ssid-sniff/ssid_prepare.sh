#!/bin/sh

systemctl stop NetworkManager.service
ip link set wlp3s0 down
ip link set wlp3s0 name wlan0
ip link set wlan0 down

cp /usr/sbin/tcpdump /tmp/
setcap cap_net_raw,cap_net_admin=eip /tmp/tcpdump

iwconfig wlan0 mode monitor
ip link set wlan0 up

